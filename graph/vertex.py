def get_successors(vertex, adjacency_dict=None):
    invalid_args = type(vertex) == OoVertex and adjacency_dict is not None
    if invalid_args:
        raise ValueError("adjacency_dict is ignored with OoVertex")

    if type(vertex) == OoVertex:
        return vertex.neighbors
    elif adjacency_dict is None:
        return []
    else:
        return adjacency_dict[vertex]


class Vertex:
    def __init__(self, name=""):
        self.name = name

    def __str__(self):
        return self.name

    __repr__ = __str__


class OoVertex(Vertex):
    """
    Object oriented implementation of Vertex
    """

    def __init__(self, name=""):
        super().__init__(name)
        self.neighbors = []
