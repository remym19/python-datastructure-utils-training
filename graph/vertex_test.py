from unittest import TestCase

from graph.vertex import Vertex, OoVertex, get_successors


class VertexSuccessorsTest(TestCase):
    def test_oo(self):
        oov = OoVertex('oov')
        n1 = OoVertex('n1')
        n2 = OoVertex('n2')
        oov.neighbors = [n1, n2]

        successors = get_successors(oov)

        self.assertEqual([n1, n2], successors)

    def test_adj(self):
        v = Vertex('v')
        n1 = Vertex('n1')
        n2 = Vertex('n2')
        adj = {v: [n1, n2]}

        successors = get_successors(v, adj)

        self.assertEqual([n1, n2], successors)

    def test_adj_not_with_oo(self):
        oov = OoVertex('oov')
        adj = {}
        self.assertRaises(ValueError, lambda: get_successors(oov, adj))
