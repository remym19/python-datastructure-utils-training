class BFS:
    """
    Breadth-First Search algorithm
    """
    def __init__(self):
        self.finished = False
        self.visit_order = []
        self.level = {}
        self.parent = {}
        self.current_level = 0

    def run_from(self, start):
        self.__init__()

        frontier = self._visit_and_get_initial_frontier(start)
        while len(frontier) > 0:
            self.current_level += 1
            frontier = self._explore_and_get_next(frontier)

        self.finished = True

    def _visit_and_get_initial_frontier(self, start):
        self._visit_from_parent(start, None)
        return [start]

    def _explore_and_get_next(self, frontier):
        next_frontier = []

        for f in frontier:
            for neighbor in f.neighbors:
                if self._to_explore(neighbor):
                    self._visit_from_parent(neighbor, f)
                    next_frontier.append(neighbor)

        return next_frontier

    def _to_explore(self, vertex):
        return vertex not in self.level

    def _visit_from_parent(self, visited_child, parent):
        self.visit_order.append(visited_child)
        self.level[visited_child] = self.current_level
        self.parent[visited_child] = parent
