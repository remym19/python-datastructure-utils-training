from unittest import TestCase

from bfs import BFS
from graph.vertex import Vertex, OoVertex


class BfsTest(TestCase):

    def setUp(self):
        super(BfsTest, self).setUp()
        self.bfs = BFS()

    def test_1_vertex(self):
        a = Vertex()
        self.bfs.run_from(a)
        self.assertListEqual([a], self.bfs.visit_order)

    def test_2_linked_vertices(self):
        a = OoVertex('a')
        b = OoVertex('b')
        a.neighbors.append(b)

        self.bfs.run_from(a)

        self.assertListEqual([a, b], self.bfs.visit_order)

    def test_3_chained_vertices(self):
        a = OoVertex('a')
        b = OoVertex('b')
        c = OoVertex('c')
        a.neighbors.append(b)
        b.neighbors.append(c)

        self.bfs.run_from(a)

        self.assertListEqual([a, b, c], self.bfs.visit_order)

    def test_3_vertices_bf_order(self):
        start = OoVertex('start')
        level1_first = OoVertex('level1_first')
        level1_second = OoVertex('level1_second')
        level2 = OoVertex('level2')
        start.neighbors.append(level1_first)
        start.neighbors.append(level1_second)
        level1_first.neighbors.append(level2)

        self.bfs.run_from(start)

        print(self.bfs.visit_order)
        self.assertListEqual([start, level1_first, level1_second, level2], self.bfs.visit_order)
