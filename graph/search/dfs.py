from graph.vertex import get_successors


class DFS:
    """
    Depth-First Search algorithm
    """

    def __init__(self):
        self.finished = False
        self.visit_order = []

    def run(self, adj):
        for s in adj:
            if s not in self.visit_order:
                self._visit_vertex(s)
                self.explore_from(s, adj)

    def explore_from(self, vertex, adj):
        for n in get_successors(vertex, adj):
            if n not in self.visit_order:
                self._visit_vertex(n)
                self.explore_from(n, adj)

        self.finished = True

    def _visit_vertex(self, vertex):
        self.visit_order.append(vertex)
