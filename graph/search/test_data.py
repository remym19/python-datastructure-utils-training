from graph.vertex import Vertex, OoVertex


def make_undir_square_vertices_adj():
    """
    Line 1: a - b

    Line 2: c - d

    There is no edge on the diagonals.
    """

    a = Vertex('start_a')
    b = Vertex('b')
    c = Vertex('c')
    d = Vertex('d')

    adj = {a: [b, c],
           b: [a, d],
           c: [a, d],
           d: [b, c]}

    return [a, b, c, d], adj


def make_undir_double_square_vertices_adj():
    """
    Line 1: a - b

    Line 2: c - d

    Line 3: e - f

    There is no edge on the diagonals.
    """
    a = Vertex('start_a')
    b = Vertex('b')
    c = Vertex('c')
    d = Vertex('d')
    e = Vertex('e')
    f = Vertex('f')

    adj = {a: [b, c],
           b: [a, d],
           c: [a, d, e],
           d: [b, c, f],
           e: [c, f],
           f: [d, e]}

    return [a, b, c, d, e, f], adj


def make_undir_square_oo_vertices():
    """
    Line 1: a - b

    Line 2: c - d

    There is no edge on the diagonals.
    """
    a = OoVertex('start_a')
    b = OoVertex('b')
    c = OoVertex('c')
    d = OoVertex('d')

    a.neighbors = [b, c]
    b.neighbors = [a, d]
    c.neighbors = [a, d]
    d.neighbors = [b, c]

    return [a, b, c, d]


def make_undir_double_square_oo_vertices():
    """
    Line 1: a - b

    Line 2: c - d

    Line 3: e - f

    There is no edge on the diagonals.
    """
    a = OoVertex('start_a')
    b = OoVertex('b')
    c = OoVertex('c')
    d = OoVertex('d')
    e = OoVertex('e')
    f = OoVertex('f')

    a.neighbors = [b, c]
    b.neighbors = [a, d]
    c.neighbors = [a, d, e]
    d.neighbors = [b, c, f]
    e.neighbors = [c, f]
    f.neighbors = [d, e]

    return [a, b, c, d, e, f]
