from functools import reduce
from unittest import TestCase

from graph.search.test_data import \
    make_undir_square_vertices_adj, \
    make_undir_double_square_vertices_adj, \
    make_undir_square_oo_vertices, \
    make_undir_double_square_oo_vertices
from graph.vertex import Vertex
from lfs import LFS


def debug_print_path_visit_order(lfs):
    print('DEBUG: path visit order')
    for p in lfs.path_visit_order:
        print(p)


class LfsPathsTest(TestCase):

    def test_undirected_square_oo(self):
        start, lfs_expected_paths = lfs_undir_square_oo()
        self._check_lfs_paths_from_oo_start(lfs_expected_paths, start)

    def test_undirected_double_square_oo(self):
        start, lfs_expected_paths = lfs_undir_double_square_oo()
        self._check_lfs_paths_from_oo_start(lfs_expected_paths, start)

    def _check_lfs_paths_from_oo_start(self, lfs_expected_paths, start):
        lfs = LFS()
        lfs.run_from([start])
        # debug_print_path_visit_order(lfs)
        self.assertEqual(lfs_expected_paths, lfs.path_visit_order)

    def test_undirected_square_adj(self):
        start, adj, expected = lfs_undir_square_adj()
        self._check_lfs_paths_from_adj_start(expected, adj, start)

    def test_undirected_double_square_adj(self):
        start, adj, expected = lfs_undir_square_adj()
        self._check_lfs_paths_from_adj_start(expected, adj, start)

    def _check_lfs_paths_from_adj_start(self, expected, adj, start):
        lfs = LFS()
        lfs.run_from([start], adj)
        # debug_print_path_visit_order(lfs)
        self.assertEqual(expected, lfs.path_visit_order)


class LfsLimitTest(TestCase):
    def test_no_path_with_limit_0(self):
        start = Vertex('start')
        other = Vertex('other')
        adj = {start: [other]}

        lfs = LFS()
        lfs.edge_count_limit = 0
        lfs.run_from([start], adj)

        self.assertEqual([], lfs.path_visit_order)

    def test_edge_path_with_limit_1(self):
        start = Vertex('start')
        v1 = Vertex('v1')
        v2 = Vertex('v2')
        adj = {start: [v1],
               v2: [v2]}

        lfs = LFS()
        lfs.edge_count_limit = 1
        lfs.run_from([start], adj)

        self.assertEqual([[start, v1]], lfs.path_visit_order)

    def test_undirected_square(self):
        start, adj, all_paths = lfs_undir_square_adj()
        self._check_all_limits(start, adj, all_paths)

    def test_undirected_double_square(self):
        start, adj, all_paths = lfs_undir_double_square_adj()
        self._check_all_limits(start, adj, all_paths)

    def _check_all_limits(self, start, adj, all_paths):
        max_edge_count = reduce(lambda a, b: max(a, b),
                                map(lambda p: len(p) - 1,
                                    all_paths))

        for edge_count_limit in range(0, max_edge_count + 1):
            self._check_within_limit(start, adj, all_paths, edge_count_limit)

    def _check_within_limit(self, start, adj, all_paths, edge_count_limit):
        lfs = LFS()
        lfs.edge_count_limit = edge_count_limit

        lfs.run_from([start], adj)

        debug_print_path_visit_order(lfs)
        print()
        within_limit = list(filter(lambda p: len(p) - 1 <= edge_count_limit, all_paths))
        self.assertEqual(within_limit, lfs.path_visit_order)


def lfs_undir_square_adj():
    [a, b, c, d], adj = make_undir_square_vertices_adj()
    return a, adj, lfs_undir_square_expected_paths(a, b, c, d)


def lfs_undir_square_oo():
    [a, b, c, d] = make_undir_square_oo_vertices()
    return a, lfs_undir_square_expected_paths(a, b, c, d)


def lfs_undir_square_expected_paths(start_a, b, c, d):
    return [[start_a, b],
            [start_a, b, d],
            [start_a, b, d, c],
            [start_a, c],
            [start_a, c, d],
            [start_a, c, d, b]]


def lfs_undir_double_square_oo():
    [a, b, c, d, e, f] = make_undir_double_square_oo_vertices()
    return a, lfs_undir_double_square_expected_paths(a, b, c, d, e, f)


def lfs_undir_double_square_adj():
    [a, b, c, d, e, f], adj = make_undir_double_square_vertices_adj()
    return a, adj, lfs_undir_double_square_expected_paths(a, b, c, d, e, f)


def lfs_undir_double_square_expected_paths(start_a, b, c, d, e, f):
    return [[start_a, b],
            [start_a, b, d],
            [start_a, b, d, c],
            [start_a, b, d, c, e],
            [start_a, b, d, c, e, f],
            [start_a, b, d, f],
            [start_a, b, d, f, e],
            [start_a, b, d, f, e, c],
            [start_a, c],
            [start_a, c, d],
            [start_a, c, d, b],
            [start_a, c, d, f],
            [start_a, c, d, f, e],
            [start_a, c, e],
            [start_a, c, e, f],
            [start_a, c, e, f, d],
            [start_a, c, e, f, d, b]]
