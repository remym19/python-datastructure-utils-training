from graph.vertex import get_successors


class LFS:
    """
    Length-First Search algorithm (LFS)

    It is analogous to depth-first search.

    From a starting vertex, LFS goes down each accessible path until either:
    - the length edge_count_limit has been reached
    - it encounters a vertex that is already on the current path
    """

    def __init__(self):
        self.edge_count_limit = None
        self.finished = False
        self.path_visit_order = []

    def run_from(self, path, adj=None):
        if len(path) - 1 == self.edge_count_limit:
            return

        tail = path[len(path) - 1]
        for n in get_successors(tail, adj):
            if n not in path:
                self._visit_vertex_from_path(n, path)
                self.run_from(path + [n], adj)

        self.finished = True

    def _visit_vertex_from_path(self, vertex, parent_path):
        vertex_path = parent_path + [vertex]
        self.path_visit_order.append(vertex_path)
