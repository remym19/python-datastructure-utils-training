from unittest import TestCase

from dfs import DFS
from graph.search.test_data import make_undir_square_vertices_adj, make_undir_double_square_vertices_adj


def debug_print_path_visit_order(dfs):
    print('DEBUG: path visit order')
    for p in dfs.visit_order:
        print(p)


class DfsOrderTest(TestCase):

    def test_undirected_square(self):
        adj, expected = dfs_undir_square()
        self._check_dfs_order_from_adj(expected, adj)

    def test_undirected_double_square(self):
        adj, expected = dfs_undir_double_square()
        self._check_dfs_order_from_adj(expected, adj)

    def test_two_disconnected_squares(self):
        adj1, expected1 = dfs_undir_square()
        adj2, expected2 = dfs_undir_square()

        adj = {}
        adj.update(adj1)
        adj.update(adj2)
        expected = expected1 + expected2

        self._check_dfs_order_from_adj(expected, adj)

    def _check_dfs_order_from_adj(self, expected, adj):
        dfs = DFS()
        dfs.run(adj)
        # debug_print_path_visit_order(dfs)
        self.assertEqual(expected, dfs.visit_order)


def dfs_undir_square():
    [a, b, c, d], adj = make_undir_square_vertices_adj()
    dfs_expected_visit_order = [a, b, d, c]
    return adj, dfs_expected_visit_order


def dfs_undir_double_square():
    [a, b, c, d, e, f], adj = make_undir_double_square_vertices_adj()
    dfs_expected_visit_order = [a, b, d, c, e, f]
    return adj, dfs_expected_visit_order
