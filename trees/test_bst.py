import unittest
from unittest import TestCase

from bst import *


def assert_key_equal(test_case: TestCase,
                     node1: BSTNode,
                     node2: BSTNode):
    unittest.TestCase.assertEqual(test_case, node1.key, node2.key)


class BstNodeInitTest(TestCase):

    def test_children_default_init(self):
        node = BSTNode()
        self.assertIsNone(node.left)
        self.assertIsNone(node.right)

    def test_children_constructor_init(self):
        left = BSTNode()
        right = BSTNode()

        node = BSTNode(None, left, right)

        self.assertEqual(left, node.left)
        self.assertEqual(right, node.right)

    def test_children_lazy_init(self):
        left = BSTNode()
        right = BSTNode()

        node = BSTNode()
        node.left = left
        node.right = right

        self.assertEqual(left, node.left)
        self.assertEqual(right, node.right)

    def test_key_default_init(self):
        node = BSTNode()
        self.assertIsNone(node.key)

    def test_key_constructor_init(self):
        key = object()

        node = BSTNode(key)

        self.assertEqual(key, node.key)

    def test_key_lazy_init(self):
        key = object()

        node = BSTNode()
        node.key = key

        self.assertEqual(key, node.key)


class BstNodeEncodeTest(TestCase):

    def test_encode_empty_node(self):
        self.assertListEqual([None, None, None],
                             BSTNode(None, None, None).encode())

    def test_encode_node_with_key_only(self):
        self.assertListEqual([5, None, None],
                             BSTNode(5).encode())

    def test_encode_node_with_left(self):
        self.assertListEqual([5, [1, None, None], None],
                             BSTNode(5, BSTNode(1), None).encode())

    def test_encode_node_with_right(self):
        self.assertListEqual([5, None, [10, None, None]],
                             BSTNode(5, None, BSTNode(10)).encode())

    def test_encode_node_with_both(self):
        self.assertListEqual([5, [1, None, None], [10, None, None]],
                             BSTNode(5, BSTNode(1), BSTNode(10)).encode())


class BstInitTest(TestCase):

    def test_root_default_init(self):
        bst = BST()

        self.assertIsNone(bst.root)

    def test_root_constructor_init(self):
        root = BSTNode()

        bst = BST(root)

        self.assertEqual(root, bst.root)

    def test_root_lazy_init(self):
        root = BSTNode()
        bst = BST()

        bst.root = root

        self.assertEqual(root, bst.root)


class BstInsertTest(TestCase):

    def test_insert_in_empty_tree(self):
        inserted = BSTNode()
        bst = BST()

        bst.insert(inserted)

        self.assertEqual(inserted, bst.root)

    def test_insert_lower_than_root(self):
        initial_root = BSTNode(10)
        bst = BST(initial_root)

        inserted = BSTNode(0)
        bst.insert(inserted)

        actual_root = bst.root
        assert_key_equal(self, initial_root, actual_root)
        assert_key_equal(self, inserted, actual_root.left)

    def test_insert_greater_than_root(self):
        initial_root = BSTNode(10)
        bst = BST(initial_root)

        inserted = BSTNode(20)
        bst.insert(inserted)

        actual_root = bst.root
        assert_key_equal(self, initial_root, actual_root)
        self.assertEqual(inserted, actual_root.right)

    def test_insert_lower_than_left_child(self):
        initial_left = BSTNode(5)
        initial_root = BSTNode(10, initial_left, None)
        bst = BST(initial_root)

        inserted = BSTNode(0)
        bst.insert(inserted)

        actual_root = bst.root
        assert_key_equal(self, initial_root, actual_root)
        assert_key_equal(self, initial_left, actual_root.left)
        assert_key_equal(self, inserted, actual_root.left.left)

    def test_insert_greater_than_left_child(self):
        initial_left = BSTNode(5)
        initial_root = BSTNode(10, initial_left, None)
        bst = BST(initial_root)

        inserted = BSTNode(7)
        bst.insert(inserted)

        actual_root = bst.root
        assert_key_equal(self, initial_root, actual_root)
        assert_key_equal(self, initial_left, actual_root.left)
        assert_key_equal(self, inserted, actual_root.left.right)

    def test_insert_greater_than_left_child_with_grand_child(self):
        initial_left_right = BSTNode(8)
        initial_left = BSTNode(5, None, initial_left_right)
        initial_root = BSTNode(10, initial_left, None)
        bst = BST(initial_root)

        inserted = BSTNode(7)
        bst.insert(inserted)

        actual_root = bst.root
        assert_key_equal(self, initial_root, actual_root)
        assert_key_equal(self, initial_left, actual_root.left)
        assert_key_equal(self, initial_left_right, actual_root.left.right)
        assert_key_equal(self, inserted, actual_root.left.right.left)

    # todo: test with a None node and a not-None node
