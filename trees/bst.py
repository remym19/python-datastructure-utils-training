"""
This module provides a Binary Search Tree data structure.
"""


class BSTNode:

    def __init__(self, key=None, left=None, right=None) -> None:
        super().__init__()
        self.key = key
        self.left = left
        self.right = right

    def __eq__(self, other: 'BSTNode'):
        return self.key == other.key

    def __lt__(self, other: 'BSTNode'):
        return self.key < other.key

    def __le__(self, other: 'BSTNode'):
        return self.key <= other.key

    def __gt__(self, other: 'BSTNode'):
        return self.key > other.key

    def __ge__(self, other: 'BSTNode'):
        return self.key >= other.key

    @property
    def key(self):
        return self.__key

    @key.setter
    def key(self, key):
        self.__key = key

    @property
    def left(self):
        return self.__left

    @left.setter
    def left(self, left):
        self.__left = left

    @property
    def right(self):
        return self.__right

    @right.setter
    def right(self, right):
        self.__right = right

    def encode(self):
        return [self.key,
                self.left.encode() if self.left is not None else None,
                self.right.encode() if self.right is not None else None]


class BST:

    def __init__(self, root=None) -> None:
        super().__init__()
        self.root = root

    @property
    def root(self) -> BSTNode:
        return self.__root

    @root.setter
    def root(self, root: BSTNode):
        self.__root = root

    # todo: check that the inserted node is accessible without returning any value from insert()
    def insert(self, node, subtree_root=None):
        # wayup: simplify
        if subtree_root is None:
            if self.root is not None:
                subtree_root = self.root  # default to self.root
            else:
                self.root = node  # initialize root if the tree is empty
                return

        if node <= subtree_root:
            if subtree_root.left is None:
                subtree_root.left = node
            else:
                self.insert(node, subtree_root.left)
        else:
            assert node > subtree_root
            if subtree_root.right is None:
                subtree_root.right = node
            else:
                self.insert(node, subtree_root.right)
